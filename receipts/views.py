from django.shortcuts import render, redirect
from receipts.models import Receipt, ExpenseCategory, Account
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from receipts.forms import CreateForm, ExpenseForm, AccountForm
from django.db.models import Count

# Create your views here.


@login_required
def list_view(request):
    receipt_list = Receipt.objects.filter(purchaser=request.user)
    context = {"receipt_list": receipt_list}

    return render(request, "receipts/home.html", context)


def redirect_home(request):
    return redirect("home")


@login_required
def create_view(request):
    if request.method == "POST":
        item_form = CreateForm(request.POST)

        if item_form.is_valid():
            item = item_form.save(commit=False)
            item.purchaser = request.user
            item.save()

            return redirect("home")
    else:
        item_form = CreateForm()
    context = {"form": item_form}
    return render(request, "receipts/create.html", context)


@login_required
def expenseListView(request):
    expense_list = ExpenseCategory.objects.filter(owner=request.user)
    expense_list = expense_list.annotate(totals=Count("receipts"))
    context = {
        "expense_list": expense_list,
    }
    return render(request, "receipts/categories.html", context)


@login_required
def accountListView(request):
    account_list = Account.objects.filter(owner=request.user)
    account_list = account_list.annotate(totals=Count("receipts"))
    context = {"account_list": account_list}
    return render(request, "receipts/accounts.html", context)


@login_required
def createExpenseCategoryView(request):
    if request.method == "POST":
        expense_form = ExpenseForm(request.POST)

        if expense_form.is_valid():
            item = expense_form.save(commit=False)
            item.owner = request.user
            item.save()

            return redirect("category_list")
    else:
        expense_form = ExpenseForm()
    context = {"form": expense_form}
    return render(request, "receipts/create_category.html", context)


@login_required
def createAccountView(request):
    if request.method == "POST":
        account_form = AccountForm(request.POST)

        if account_form.is_valid():
            item = account_form.save(commit=False)
            item.owner = request.user
            item.save()

            return redirect("account_list")
    else:
        account_form = AccountForm()
    context = {"form": account_form}
    return render(request, "receipts/create_account.html", context)
