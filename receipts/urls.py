from django.urls import path
from receipts.views import (
    list_view,
    redirect_home,
    create_view,
    expenseListView,
    accountListView,
    createExpenseCategoryView,
    createAccountView,
)
from django.shortcuts import redirect

urlpatterns = [
    path("", list_view, name="home"),
    path("receipts/", list_view),
    path("create/", create_view, name="create_receipt"),
    path("categories/", expenseListView, name="category_list"),
    path("accounts/", accountListView, name="account_list"),
    path(
        "categories/create/", createExpenseCategoryView, name="create_category"
    ),
    path("accounts/create/", createAccountView, name="create_account"),
]
