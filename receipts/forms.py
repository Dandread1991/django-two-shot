from django import forms
from receipts.models import Receipt, ExpenseCategory, Account


class CreateForm(forms.ModelForm):
    class Meta:
        model = Receipt
        fields = ["vendor", "total", "tax", "date", "category", "account"]
        widgets = {
            "vendor": forms.TextInput(attrs={"placeholder": "Vendor Name"}),
            "total": forms.TextInput(attrs={"placeholder": "Total Amount"}),
            "tax": forms.TextInput(attrs={"placeholder": "Amount of Tax"}),
            "date": forms.DateInput(attrs={"placeholder": "Date"}),
        }


class ExpenseForm(forms.ModelForm):
    class Meta:
        model = ExpenseCategory
        fields = ["name"]
        widgets = {
            "name": forms.TextInput(attrs={"placeholder": "Name"}),
        }


class AccountForm(forms.ModelForm):
    class Meta:
        model = Account
        fields = ["name", "number"]
        widgets = {
            "name": forms.TextInput(attrs={"placeholder": "Name"}),
            "number": forms.TextInput(attrs={"placeholder": "Number"}),
        }
