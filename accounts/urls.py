from django.urls import path
from accounts.views import LoginformView, LogoutView, SignUpFormView
from django.shortcuts import redirect
from receipts.views import create_view

urlpatterns = [
    path("login/", LoginformView, name="login"),
    path("logout/", LogoutView, name="logout"),
    path("signup/", SignUpFormView, name="signup"),
    path("create/", create_view),
]
